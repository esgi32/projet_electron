const calculateurCo2 = require('../script/calculateur_co2');

describe('calculateurCo2', () => {
  test('Calcule les émissions de CO2 pour une voiture', () => {
    const kmUtilisateur = 100;
    const consommationCo2Voiture = 200;
    const expected = 20000;
    expect(calculateurCo2(kmUtilisateur, consommationCo2Voiture)).toBe(
      expected
    );
  });

  test('Calcule les émissions de CO2 pour un bus', () => {
    const kmUtilisateur = 150;
    const consommationCo2Bus = 150;
    const expected = 22500;
    expect(calculateurCo2(kmUtilisateur, consommationCo2Bus)).toBe(expected);
  });

  test('Calcule les émissions de CO2 pour un train', () => {
    const kmUtilisateur = 200;
    const consommationCo2Train = 100;
    const expected = 20000;
    expect(calculateurCo2(kmUtilisateur, consommationCo2Train)).toBe(expected);
  });
  test('Calcule les émissions de CO2 pour un avion', () => {
    const kmUtilisateur = 1000;
    const consommationCo2Avion = 130;
    const expected = 130000;
    expect(calculateurCo2(kmUtilisateur, consommationCo2Avion)).toBe(expected);
  });
});
