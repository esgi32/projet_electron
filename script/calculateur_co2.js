/** calcule les émissions de CO2 pour une voiture en multipliant la distance en kilomètres par la consommation de CO2 du moyen de véhicule */
function calculateurCo2(kmUtilisateur, consommationCo2) {
  return kmUtilisateur * consommationCo2;
}

module.exports = calculateurCo2;
