// import { selectTransport } from './choixTransport';
// import { calculateurCo2 } from './calculateur_co2';
const fs = require('fs');
const { id } = require('../persistance');
const historique = require('../historique.json');

const API_KEY = '1624a5b73c8fe1f9cd7bddaf70037b98';
const API_URL = `https://api.openweathermap.org/data/2.5/weather?q=cityName&appid=${API_KEY}&lang=fr`;
const divElement = document.createElement('div');
const sectionList = document.getElementById('sectionList');
sectionList.appendChild(divElement);

/** Affiche-les élements en cas de connexion ou non connexion  */

const btnConnexion = document.getElementById('btn-connexion');
const btnDeconnexion = document.getElementById('btn-deconnexion');
const btncreation = document.getElementById('join-btn');
btnDeconnexion.style.display = 'none';
btncreation.style.display = 'none';
const divList = document.getElementById('isNotConnected');
const inputConnection = document.getElementById('token');
// const targetDiv = document.getElementById('isConnected');
const auteurSection = document.getElementById('auteurConnecter');
const distanceSection = document.getElementById('isConnectDistance');
const transportDistance = document.getElementById('isConnectedTransport');
const auteurText = document.createElement('h3');
const value1 = id[0].token;
const value2 = id[1].token;

const intPutTransport = document.getElementById('transport');
const intPutDistance = document.getElementById('distance');

/** Lis le json */
function getList() {
  const listArray = [];
  historique.forEach((item) => {
    listArray.push(item);
  });
  return listArray;
}

/** Recuperer l'historique des trajets */

function createList() {
  const items = getList();
  items.forEach((item, index) => {
    divElement.setAttribute('id', `${index + 1}`);
    divElement.classList.add('sectionList');

    const nomElemnt = document.createElement('h3');
    nomElemnt.innerHTML = `Trajet ${index + 1} : `;
    divElement.appendChild(nomElemnt);

    const ulElement = document.createElement('ul');
    divElement.appendChild(ulElement);

    const transportLi = document.createElement('li');
    transportLi.innerHTML = `Choix de transport: ${item.choix_transport}`;
    ulElement.appendChild(transportLi);

    const distanceLi = document.createElement('li');
    distanceLi.innerHTML = `Distance parcourue: ${item.distance_parcouru}`;
    ulElement.appendChild(distanceLi);

    const dateLi = document.createElement('li');
    dateLi.innerHTML = `Date: ${item.date}`;
    ulElement.appendChild(dateLi);

    const co2Li = document.createElement('li');
    co2Li.innerHTML = `Totale CO2: ${item.co2}`;
    ulElement.appendChild(co2Li);

    const auteurLi = document.createElement('li');
    auteurLi.innerHTML = `Auteur : ${item.auteur}`;
    ulElement.appendChild(auteurLi);

    const separationElement = document.createElement('hr');
    ulElement.appendChild(separationElement);
  });
}

function calculateurCo2({ consommationTransportCo2, valueDistance }) {
  return consommationTransportCo2 * valueDistance;
}

createList();

/** Créer un élement dans historique */

function createPesistance(data) {
  fs.writeFile('historique.json', JSON.stringify(data, null, 4), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('File created');
    }
  });
}

/**  gere la deconnexion au cliqué sur le bouton */
btnDeconnexion.addEventListener('click', () => {
  auteurText.remove();
  divList.classList.add('isNotConnected');
  divList.style.display = 'none';
  distanceSection.style.display = 'none';
  transportDistance.style.display = 'none';
  btnDeconnexion.style.display = 'none';
  btncreation.style.display = 'none';
  btnConnexion.style.display = 'inline-block';
  inputConnection.disabled = false;
});
btnConnexion.addEventListener('click', () => {
  const inputValue = document.getElementById('token').value;
  if (inputValue === value1) {
    auteurText.remove();
    auteurText.innerHTML = `Vous êtes connecté en tant que ${id[0].nom}`;
    divList.classList.remove('isNotConnected');
    auteurSection.appendChild(auteurText);
    divList.style.display = 'block';
    distanceSection.style.display = 'block';
    transportDistance.style.display = 'block';
    btnDeconnexion.style.display = 'inline-block';
    btncreation.style.display = 'inline-block';
    btnConnexion.style.display = 'none';
    inputConnection.disabled = true;
  } else if (inputValue === value2) {
    auteurText.remove();
    auteurText.innerHTML = `Vous êtes connecté en tant que ${id[1].nom}`;
    divList.classList.remove('isNotConnected');
    auteurSection.appendChild(auteurText);
    divList.style.display = 'block';
    distanceSection.style.display = 'block';
    transportDistance.style.display = 'block';
    btnDeconnexion.style.display = 'inline-block';
    btncreation.style.display = 'inline-block';
    btnConnexion.style.display = 'none';
    inputConnection.disabled = true;
  } else {
    alert('La valeur saisie est incorrecte');
  }
});

/** generele resultat du calcul CO2 */

function switchTransport() {
  const valueTransport = document.getElementById('transport').value;
  const valueDistance = parseInt(document.getElementById('distance').value);
  let consommationTransportCo2 = 0;
  switch (valueTransport) {
    case 'avion':
      consommationTransportCo2 = 130;
      return calculateurCo2({ consommationTransportCo2, valueDistance });
    case 'bus':
      consommationTransportCo2 = 150;
      return calculateurCo2({ consommationTransportCo2, valueDistance });
    case 'train':
      consommationTransportCo2 = 100;
      return calculateurCo2({ consommationTransportCo2, valueDistance });

    case 'voiture':
      consommationTransportCo2 = 200;
      return calculateurCo2({ consommationTransportCo2, valueDistance });
    default:
      console.log('Erreur saisie');
      break;
  }
}

async function getWeatherIcon(city) {
  const response = await fetch(API_URL.replace('cityName', city));
  const data = await response.json();
  const iconId = data.weather[0].icon;
  const iconUrl = `http://openweathermap.org/img/wn/${iconId}@2x.png`;
  return iconUrl;
}

// async function getWeatherDescription(city) {
//   const response = await fetch(API_URL.replace("cityName", city));
//   const data = await response.json();
//   const { description } = data.weather[0];
//   return description;
// }

getWeatherIcon('Grenoble').then((iconUrl) => {
  const meteo = document.getElementById('meteo');
  meteo.innerHTML = `<img src="${iconUrl}" alt=" image de la météo"/>`;
});
btncreation.addEventListener('click', () => {
  const nouvelElement = {
    choix_transport: intPutTransport.value,
    distance_parcouru: intPutDistance.value,
    date: Date.now(),
    co2: switchTransport(),
    auteur: inputConnection.value,
  };
  historique.push(nouvelElement);
  createPesistance(historique);
  createList();
});
