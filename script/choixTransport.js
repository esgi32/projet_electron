const { co2 } = require('../persistance');

function selectTransport() {
  // Récupère l'élément select
  const select = document.getElementById('transport');

  // Ajoute un écouteur d'événements pour détecter un clic sur l'élément select
  select.addEventListener('change', (event) => {
    // Récupère la valeur sélectionnée dans l'élément select
    const selectedVehicle = event.target.value;

    // Vérifie la valeur sélectionnée et effectue une action en conséquence
    switch (selectedVehicle) {
      case 'Train':
        console.log(`Train selected ${co2[0].train}`);
        break;
      case 'Voiture':
        console.log(`Voiture selected ${co2[0].voiture}`);
        break;
      case 'Bus':
        console.log(`Bus selected ${co2[0].bus}`);
        break;
      case 'Avion':
        console.log(`Avion selected ${co2[0].avion}`);
        break;
      default:
    }
    console.log(selectedVehicle);
  });
}
module.exports = {
  selectTransport,
};
