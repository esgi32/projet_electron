const { app, BrowserWindow } = require('electron');
require('electron-reload')(__dirname);

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    },
  });

  win.loadFile('./views/main.html');
};
app.whenReady().then(() => {
  createWindow();
});
