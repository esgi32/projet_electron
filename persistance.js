/**
 Ce code est un module JavaScript qui exporte trois tableaux d'objets.

 Le tableau "id" contient deux objets représentant des utilisateurs avec un token, un mot de passe et un nom.

 Le tableau "co2" contient un objet qui liste les émissions de CO2 de différents modes de transport.

 Le tableau "historique" contient cinq objets représentant des enregistrements historiques des choix de transport, des distances parcourues, des dates, des émissions de CO2 et des auteurs.

 Ces tableaux et objets sont ensuite exportés et peuvent être utilisés par d'autres parties du programme.
 */

const id = [
  {
    token: '123456789',
    mot_de_passe: 'admin',
    nom: 'Cedric Kadima',
  },
  {
    token: '987654321',
    mot_de_passe: 'root',
    nom: 'Hugo Rodrigues',
  },
];

const co2 = [
  {
    avion: 130,
    bus: 150,
    train: 100,
    voiture: 200,
  },
];
module.exports = {
  id,
  co2,
};
